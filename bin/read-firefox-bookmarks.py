#!/usr/bin/env python3
import argparse
from contextlib import contextmanager
from datetime import date
import csv
import tempfile
import os
import re
import shutil
import sqlite3
import sys

def debug_list_tables(cxn):
    '''List the tables in a given SQL database. For debugging when/if Firefox's
    database schema changes'''
    cur = cxn.cursor()
    cur.execute('''SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%' ORDER BY 1;''')
    return [row[0] for row in cur.fetchall()]

def debug_list_columns(cxn, table_name):
    '''List the columns in a given SQLite table. For debugging when/if
    Firefox's database schema changes'''
    cur.execute('SELECT * FROM {}'.format(table_name))
    return cur.fetchone().keys()

@contextmanager
def temporary_copy(filename):
    '''Create a temporary copy of a file. This is necessary because Firefox
    locks the SQLite database, and clients refuse to query it in this state.'''
    temp_directory = tempfile.mkdtemp(prefix='firefox-places-')
    temp_filename = os.path.join(temp_directory, 'places.sqlite')

    shutil.copy2(filename, temp_filename)

    yield temp_filename

    shutil.rmtree(temp_directory)

def find_firefox_database():
    firefox_dir = os.path.join(os.environ['HOME'], '.mozilla', 'firefox')

    for item in os.listdir(firefox_dir):
        if re.match(r'^\w+\.default(-esr)?$', item):
            path = os.path.join(firefox_dir, item, 'places.sqlite')
            if os.path.exists(path):
                return path

    raise Exception('Could not find Firefox database file ("places.sqlite")')

def get_firefox_bookmarks_by_folder(sqlite_file, folder_name):
    query = '''
        SELECT moz_bookmarks.dateAdded, moz_bookmarks.title, moz_places.url
        FROM moz_bookmarks, moz_places, moz_bookmarks AS parent
        WHERE moz_bookmarks.fk=moz_places.id AND moz_bookmarks.parent=parent.id AND parent.title="{}"
    '''.format(folder_name)
    cxn = sqlite3.connect(sqlite_file)

    try:
        cxn.row_factory = sqlite3.Row

        cur = cxn.cursor()

        for row in cur.execute(query):
            yield row
    finally:
        cxn.close()

def main(folder_name):
    database_file = find_firefox_database()

    with temporary_copy(database_file) as temp_file:
        writer = csv.writer(sys.stdout)

        for bookmark in get_firefox_bookmarks_by_folder(temp_file, folder_name):
            timestamp_ms = bookmark['dateAdded'] / 10 ** 6
            date_str = date.strftime(date.fromtimestamp(timestamp_ms), '%Y-%m-%d')
            writer.writerow((date_str, bookmark['title'], bookmark['url']))

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(
            'Expecting exactly one command-line argument specifying the name of the bookmark folder',
            file=sys.stderr
        )
        sys.exit(1)

    if sys.argv[1] in ('-h', '--help'):
        print(
            'Print the Firefox bookmarks for the current user, limited to a specific '
            'bookmark folder and formatted in CSV. (Hint: unsorted bookmarks may be '
            'stored in a folder named "unfiled".)'
        )
        sys.exit(0)

    main(sys.argv[1])
