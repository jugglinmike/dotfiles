--[[
Media Playback History Extension for VLC media player
Copyright 2016 - 2017 Reiuiji
Copyright 2023 jugglinmike

Authors: Reiuiji, jugglinmike
Contact: reiuiji@gmail.com

Information:
Keeps track of which songs were played in VLC and saves the song information as
well as the time played to a CSV-formatted text file.

-------------------------
Installation Instructions
-------------------------

Place this file in the corresponding folder and restart VLC or reload plugin
extensions.

Linux:
  Current User: ~/.local/share/vlc/lua/extensions/
     All Users: /usr/lib/vlc/lua/extensions/

Windows:
  Current User: %APPDATA%\vlc\lua\extensions
     All Users: %ProgramFiles%\VideoLAN\VLC\lua\extensions\

Mac OS X:
  Current User: /Users/%your_name%/Library/Application Support/org.videolan.vlc/lua/extensions/
     All Users: /Applications/VLC.app/Contents/MacOS/share/lua/extensions/

The SongList.csv will be saved in a directory named "media-playback-history"
within the current user's "HOME" directory.

--]]

-- Global Variables
-- Name of the file to save the songs
SongTrackerDirectoryPattern = "__HOME__/Documents/media-playback-history"
SongTrackerFilePattern = "__DATE__.csv"
slash = "/"
SongTrackerDirectory = ""
-- Default header for the csv file
FileHeader = "Date,Time,Artist,Album,Title\n"
-- CSV field separator
CSV_FS = ","
-- maintain last song played
lastsong = ""

-- Descriptor
function descriptor()
  return {
    title = "Media Playback History 1.0.0",
    version = "1.0.0",
    author = "jugglinmike",
    url = "https://gitlab.com/jugglinmike/dotfiles",
    shortdesc = "Media playback history",
    description = "Keeps track of what songs were played in VLC and saves the meta information and time played to a CSV-formatted text file.",
    capabilities = { "input-listener" }
  }
end

function debug(message)
  vlc.msg.dbg("[Media Playback History] " .. message)
end

-- Activate plugin
function activate()
  debug("Activate")
  init()
  update_song_Tracker()
end

-- Deactivate Plugin
function deactivate()
  debug("Deactivate")
end

-- Close Trigger
function close()
  debug("Close")
end

-- Triggers
function input_changed()
  debug("Input Changed")
  update_song_Tracker()
end

function meta_changed()
  debug("Meta Changed")
  update_song_Tracker()
end

-- First time init file
function init()
  debug("Initializing plugin")
  --Determine which operating system
  if string.match(vlc.config.datadir(), "^(%a:.+)$") then --Windows Detected
    debug("OS Detected: Windows")
    slash = "\\"
  elseif string.find(vlc.config.datadir(), 'MacOS') then -- Mac Detected
    debug("OS Detected: Mac OS X")
    slash = "/"
  else -- Linux/UNIX
    debug("OS Detected: Linux/Unix")
    slash = "/"
  end

  SongTrackerDirectory = string.gsub(
    SongTrackerDirectoryPattern, "/", slash
  )
  SongTrackerDirectory = string.gsub(
    SongTrackerDirectory, "__HOME__", os.getenv("HOME")
  )

  if slash == "/" then
    os.execute(string.format("mkdir -p %s", SongTrackerDirectory))
  else
    os.execute(string.format("mkdir %s", SongTrackerDirectory))
  end
  debug("Created directory: " .. SongTrackerDirectory)
end

function csv_escape(value)
  local needs_escaping = string.find(value, ",") or
    (string.len(value) > 0 and value[1] == "\"")

  if not needs_escaping then
    return value
  end

  return string.format(
    "\"%s\"",
    string.gsub(value, "\"", "\"\"")
  )
end

function extract_for_csv(metadata, attribute)
  if metadata[attribute] == nil then
    return ""
  end

  return csv_escape(metadata[attribute])
end

function is_empty(value)
  return value == nil or value == ""
end

-- Update Tracker
function update_song_Tracker()
  debug("Update Song Tracker")

  if vlc.input.is_playing() then
    local item = vlc.item or vlc.input.item()
    if item then
      local meta = item:metas()
      --Check Meta tags
      if meta then
        local artist = extract_for_csv(meta, "artist")
        local album = extract_for_csv(meta, "album")
        local title = extract_for_csv(meta, "title")

        -- I don't understand VLC's event system well enough to understand why
        -- this condition occurs, but it definitely does.
        if is_empty(artist) and is_empty(album) and is_empty(title) then
          return false
        end

        -- Combine Song Info Together
        local songinfo = artist .. CSV_FS .. album .. CSV_FS .. title

        -- Check if the song was previously played
        if lastsong == songinfo then
          debug("Duplicate song info detected, skipping")
          return false
        else
          -- New Song Detected
          -- Update last song captured
          lastsong = songinfo
          -- Date & Time
          local date = os.date("%Y-%m-%d")
          local time = os.date("%H:%M:%S")

          local SongTrackerFile = string.gsub(
            SongTrackerFilePattern, "__DATE__", os.date("%Y-%m-%d")
          )
          local SongTrackerFilePath = string.format(
            "%s%s%s", SongTrackerDirectory, slash, SongTrackerFile
          )

          -- Add time stamp with the song info
          local info = date .. CSV_FS .. time .. CSV_FS .. songinfo
          write_file(SongTrackerFilePath, info)
          return true
        end
      end
    end
  end
end

-- Write File
function write_file(name, info)
  local file = io.open(name, "r")

  if file == nil then
    debug("File does not exist, creating header")
    file = io.open(name, "w+")
    file:write(FileHeader)
  end

  file:close()
  file = io.open(name, "a")

  debug("Write File")
  file:write(info .. "\n")
  file:close()
end
