# dotfile

Personal configuration files

## License

Copyright 2021 Mike Pennisi under [the GNU General Public License
v3.0](https://www.gnu.org/licenses/gpl-3.0.html)
